from string import ascii_lowercase as letter_list
from collections import Counter
# from os.path import exists


class WordClass:
    good_letters = []
    bad_letters = []
    current_words = []
    master_words = []
    current_guess = ''

    def __init__(self):
        with open('wordle_words.txt') as f:
            for line in f:
                self.master_words.append(line.strip())

        self.reset()

    def reset(self):
        self.current_words = [word for word in self.master_words]
        self.good_letters = ['*', '*', '*', '*', '*']
        self.bad_letters = []
        self.current_guess = ''

    def get_bad_letters(self):
        return "".join(sorted(self.bad_letters))

    def get_good_letters(self):
        return "".join(self.good_letters)

    def get_word_count(self):
        return len(self.current_words)

    def delete_suggestion(self):
        self.master_words.remove(self.current_guess)
        

    def add_bad_letters(self, new_letters):
        valid_words = []
        for l in new_letters:
            if l not in self.bad_letters:
                self.bad_letters.append(l)
                for word in self.current_words:
                    if l not in word:
                        valid_words.append(word)
                self.current_words = [word for word in valid_words]
                valid_words = []

    def process_good_letters(self, guess):
        valid_words = []
        for i, l in enumerate(guess):
            if l != "*" and self.good_letters[i] == "*":
                self.good_letters[i] = l
                for word in self.current_words:
                    if word[i] == l:
                        valid_words.append(word)
                self.current_words = [word for word in valid_words]
                valid_words = []

    def process_valid_letters(self, guess):
        for i, l in enumerate(guess):
            if l != "*":
                no_bad_letter = []
                valid_words = []
                # step one - remove all words where the letter is in the wrong place
                for word in self.current_words:
                    if word[i] != l:
                        no_bad_letter.append(word)
                
                # step two - make sure that letter exists elsewhere in the word
                for word in no_bad_letter:
                    if l in word:
                        valid_words.append(word)
                        
                self.current_words = [word for word in valid_words]

    def next_guess(self):
        # object here is to go through and see what the most common letter that is missing
        # ex: if our first letter is not good then of all the remaining words, what are the 3 most common letters remaining 
        # (that are not elsewhere) being used, and do so for each missing spot
        # from there we form a valid word using the most common missing letters in each spot
        letter_options = []
        for i in range(5):
            if self.good_letters[i] != "*":
                letter_options.append(self.good_letters[i])
            else:
                letter_count = dict.fromkeys(letter_list, 0)
                for word in self.current_words:
                    letter_count[word[i]] += 1
                
                temp = Counter(letter_count)
                top = temp.most_common(3)
                letter_options.append([top[0][0], top[1][0], top[2][0]])
        
        # now our letter options are either the most used letter in each spot, or the actual letter in that spot
        guesses = [word for word in self.current_words]
        valid_words = []
        for i in range(5):
            if self.good_letters[i] == "*":
                for guess in guesses:
                    if guess[i] in letter_options[i]:
                        valid_words.append(guess)
                if len(valid_words) > 0:    # if there are any valid words, then we'll reset using them - if not, skip and keep going
                    guesses = valid_words
                valid_words = []

        self.current_guess = guesses[0]
        return self.current_guess

    def remove_invalid_guess(self):
        # current guess is not a valid word guess according to the puzzle - so we need to remove it
        pass

    def add_viable_word(self, word):
        # add a new guess to the master word list
        pass
    

def get_guess(guess):
    # guess is an instance of our word class
    temp = input(f'Enter any new bad letters ({guess.get_bad_letters()}): ').lower()
    if temp.strip() != "":
        guess.add_bad_letters(temp)
    
    print(f"Remember to fill all blanks with either an asterisk(*) or hyphen(-) or just hit enter if nothing has changed")

    temp = input(f'Enter correctly positioned letters ({guess.get_good_letters()}): ').lower()
    if '-' in temp:
        temp = temp.replace("-", "*")
    if temp != guess.get_good_letters():
        guess.process_good_letters(temp)

    temp = input(f'Enter any valid letters in the wrong location: ').lower()
    if temp.strip() != "":
        if '-' in temp:
            temp = temp.replace("-", "*")
        guess.process_valid_letters(temp)
    if guess.get_word_count() > 25:
        print(f"\nThere are {guess.get_word_count()} viable words.  My suggestion is {guess.next_guess()}")
    else:
        print(f"\nThere are {guess.get_word_count()} viable words.  They are {' * '.join(guess.current_words)}")


def print_menu():
    print(f"**********************************************************************")
    print(f"* R or r to Restart with a new word                                  *")
    print(f"* A or a to Add a new word (solved) to the master list               *")
    print(f"* X or x to Delete the (invalid) suggested word from the master list *")
    print(f"* Q or q to Quit the game                                            *")
    print(f"* Enter (or anything else) to continue                               *")
    print(f"**********************************************************************")


def main():
    guess = WordClass()
    temp = ''
    while temp != 'Q' and temp != 'q':
        if temp in ('R', 'r'):
            guess.reset()
        if temp in ('A', 'a'):
            pass
        if temp in ('X', 'x'):
            pass
        get_guess(guess)
        print_menu()
        temp = input('Selected Option: ')


if __name__ == '__main__':
    main()


